﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PortfolioApplication.Startup))]
namespace PortfolioApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
